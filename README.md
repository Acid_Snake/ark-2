ARK-2 source code.

You must acquire a decrypted version of the following PRX modules from the PSP unless otherwise stated (don't ask how) and copy them to the corresponding paths:

interruptman.prx -> contrib/PSP/f0-kd-interruptman.prx
mediasync.prx -> contrib/PSP/f0-kd-mediasync.prx
modulemgr.prx -> contrib/PSP/f0-kd-modulemgr.prx
np9660.prx -> contrib/PSP/f0-kd-np9660.prx
popsman.prx -> contrib/PSP/popsman_psp.prx
pops.prx -> contrib/PSP/pops_psp.prx
vita's popsman.prx -> contrib/PSP/f0-kd-popsman.prx
libpspvmc.prx -> contrib/PSP/f0-vsh-module-libpspvmc.prx
npdrm.prx -> contrib/PSP/psp-npdrm.prx
vita's pops.prx -> contrib/PSP/pops.prx
